#include <chrono>
#include <iomanip>
#include "LogPrinter.h"

void ::LogPrinter::print(const std::string &message) {
    auto time = std::time(nullptr);
    auto localTime = std::localtime(&time);

    std::clog << std::put_time(localTime, "%H:%M:%S: ") << message << std::endl;
}
