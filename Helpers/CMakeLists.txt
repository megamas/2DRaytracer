#Helpers
cmake_minimum_required(VERSION 3.7)
project(SDL_RayTracer)


set(CMAKE_CXX_STANDARD 14)
set(SOURCE_FILES
        LogPrinter.h LogPrinter.cpp)

add_library(Helpers STATIC ${SOURCE_FILES})