#ifndef SDL_RAYTRACER_LOGPRINTER_H
#define SDL_RAYTRACER_LOGPRINTER_H
#include <iostream>

namespace LogPrinter {
    void print(const std::string &message);
};


#endif //SDL_RAYTRACER_LOGPRINTER_H
