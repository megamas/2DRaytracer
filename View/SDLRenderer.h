#ifndef SDL_RAYTRACER_SDLRENDERER_H
#define SDL_RAYTRACER_SDLRENDERER_H
#include "Renderer.h"
#include <SDL2/SDL.h>
#include "../CustomDeleters/SDLRendererDeleter.h"
#include "../CustomDeleters/SDLWindowDeleter.h"
#include "../Model/Stage.h"
#include <memory>
#include <iostream>

class SDLRenderer : public Renderer {
protected:
    std::unique_ptr<SDL_Window, Deleters::SDLWindowDeleter> mWindow;
    std::unique_ptr<SDL_Renderer, Deleters::SDLRendererDeleter> mRenderer;
    std::shared_ptr<Stage> mStage;
    std::shared_ptr<RayCaster> mRayCaster;
    bool mEnabled;

public:
    SDLRenderer(int width, int height, bool enabled);
    virtual ~SDLRenderer();

    virtual void disable() override;
    virtual void enable() override;
    virtual void resize(int width, int height) override;
    virtual void render() override;

    void setStage(std::shared_ptr<Stage> stage) override;
    void setRayCaster(std::shared_ptr<RayCaster> raycaster) override;
};


#endif //SDL_RAYTRACER_SDLRENDERER_H
