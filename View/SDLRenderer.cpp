#include "SDLRenderer.h"
#include "../Helpers/LogPrinter.h"

SDLRenderer::SDLRenderer(int width, int height, bool enabled)
        : mEnabled(enabled) {
    mWindow.reset(SDL_CreateWindow(
            "2D RayTracer",
            SDL_WINDOWPOS_CENTERED,
            SDL_WINDOWPOS_CENTERED,
            width,
            height,
            SDL_WINDOW_OPENGL));

    mRenderer.reset(SDL_CreateRenderer(mWindow.get(), -1, SDL_RENDERER_ACCELERATED));

    if(!mRenderer) {
        throw std::runtime_error("SDL renderer was not created");
    }

    LogPrinter::print("Main renderer object created");
}

void SDLRenderer::disable() {
    mEnabled = false;
}

void SDLRenderer::enable() {
    mEnabled = true;
}

void SDLRenderer::resize(int width, int height) {

}

void SDLRenderer::render() {
    if(!mEnabled)
        return;

    SDL_RenderClear(mRenderer.get());
    if(mStage)
        mStage->draw(mRenderer.get());
    if(mRayCaster)
        mRayCaster->draw(mRenderer.get());
    SDL_RenderPresent(mRenderer.get());
}

SDLRenderer::~SDLRenderer() {
    LogPrinter::print("Main renderer destroyed...");
}

void SDLRenderer::setStage(std::shared_ptr<Stage> stage) {
    mStage = stage;
}

void SDLRenderer::setRayCaster(std::shared_ptr<RayCaster> raycaster) {
    mRayCaster = raycaster;
}
