#ifndef SDL_RAYTRACER_RENDERER_H
#define SDL_RAYTRACER_RENDERER_H

#include "../Model/Stage.h"
#include "../Model/RayCaster.h"
#include <memory>

class Renderer {
public:
    virtual void setStage(std::shared_ptr<Stage> stage) = 0;
    virtual void setRayCaster(std::shared_ptr<RayCaster> raycaster) = 0;

    virtual void disable() = 0;
    virtual void enable() = 0;

    virtual void resize(int width, int height) = 0;
    virtual void render() = 0;

    virtual ~Renderer(){};
};

#endif //SDL_RAYTRACER_RENDERER_H
