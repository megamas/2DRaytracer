#include <iostream>
#include <chrono>
#include "SDLWindowDeleter.h"
#include "../Helpers/LogPrinter.h"

namespace Deleters {
    void SDLWindowDeleter::operator()(SDL_Window *pWindow) {
        SDL_DestroyWindow(pWindow);
        LogPrinter::print("Internal window destroyed...");
    }
}
