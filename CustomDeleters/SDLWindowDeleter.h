#ifndef SDL_RAYTRACER_SDLWINDOWDELETER_H
#define SDL_RAYTRACER_SDLWINDOWDELETER_H
#include <SDL2/SDL.h>

namespace Deleters {
    class SDLWindowDeleter {
    public:
        void operator()(SDL_Window *pWindow);
    };
}


#endif
