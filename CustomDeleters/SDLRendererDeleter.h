#ifndef SDL_RAYTRACER_SDLRENDERERDELETER_H
#define SDL_RAYTRACER_SDLRENDERERDELETER_H
#include <SDL2/SDL.h>

namespace Deleters {
    class SDLRendererDeleter {
    public:
        void operator()(SDL_Renderer *pRender);
    };
}


#endif //SDL_RAYTRACER_SDLRENDERERDELETER_H
