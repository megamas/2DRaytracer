#include <iostream>
#include "SDLRendererDeleter.h"
#include "../Helpers/LogPrinter.h"

namespace Deleters {
    void SDLRendererDeleter::operator()(SDL_Renderer *pRender) {
        SDL_DestroyRenderer(pRender);
        LogPrinter::print("Internal renderer destroyed...");
    }
}
