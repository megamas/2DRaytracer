#include <iostream>
#include "RayCaster.h"
//#include <cmath>

RayCaster::RayCaster(std::shared_ptr<Stage> stage, int posX, int posY)
        : mStage(stage),
          mPosX(posX),
          mPosY(posY) {

}

RayCaster::~RayCaster() {

}

void RayCaster::draw(SDL_Renderer *renderer) {
    SDL_RenderDrawPoint(renderer, mPosX, mPosY);
    SDL_Color color;

    for(int angle = 0; angle < 360; angle += 10)
    {
        Point point = mStage->findClosestIntersection(Point(mPosX, mPosY), angle);
        color.r = 255;
        color.g = 0;
        color.b = 0;
        color.a = 255;

        SDL_GetRenderDrawColor(renderer, &color.r, &color.g, &color.b, &color.a);
        SDL_RenderDrawLine(renderer, mPosX, mPosY, point.x, point.y);

        color.r = 255;
        color.g = 255;
        color.b = 255;
        color.a = 255;
        SDL_GetRenderDrawColor(renderer, &color.r, &color.g, &color.b, &color.a);
    }
}

void RayCaster::updatePos(int posX, int posY) {
    mPosX = posX;
    mPosY = posY;
}

std::pair<Point, Point> RayCaster::getEdge(int edgeNum) {
    return std::pair<Point, Point>(Point(0, 0), Point(0, 0));
}

int RayCaster::getEdgeCount() {
    return 0;
}
