#include "Stage.h"
#include "DrawableTriangle.h"
#include "../Helpers/LogPrinter.h"
#include "DrawableLine.h"


Stage::Stage(int witdh, int height)
        : mWidth(witdh),
          mHeight(height) {

    std::unique_ptr<Drawable> polygon1(new DrawableTriangle(Point(40, 40), Point(160,250), Point(120, 80), 1));
    std::unique_ptr<Drawable> polygon2(new DrawableTriangle(Point(143, 41), Point(386, 33), Point(257, 224), 2));
    std::unique_ptr<Drawable> polygon3(new DrawableTriangle(Point(214, 251), Point(62, 391), Point(200, 578), 3));
    std::unique_ptr<Drawable> polygon4(new DrawableTriangle(Point(145, 616), Point(90, 713), Point(394, 729), 4));
    std::unique_ptr<Drawable> polygon5(new DrawableTriangle(Point(499, 240), Point(362, 315), Point(548, 418), 5));
    std::unique_ptr<Drawable> polygon6(new DrawableTriangle(Point(313, 265), Point(438, 25), Point(681, 119), 6));
    std::unique_ptr<Drawable> polygon7(new DrawableTriangle(Point(919, 73), Point(802, 354), Point(980, 314), 7));
    std::unique_ptr<Drawable> polygon8(new DrawableTriangle(Point(962, 457), Point(715, 702), Point(973, 727), 8));
    std::unique_ptr<Drawable> polygon9(new DrawableTriangle(Point(510, 718), Point(391, 523), Point(656, 720), 9));
    std::unique_ptr<Drawable> polygon10(new DrawableLine(Point(600, 200), Point(500,550), 10));

    mPolygons.push_back(std::move(polygon1));
    mPolygons.push_back(std::move(polygon2));
    mPolygons.push_back(std::move(polygon3));
    mPolygons.push_back(std::move(polygon4));
    mPolygons.push_back(std::move(polygon5));
    mPolygons.push_back(std::move(polygon6));
    mPolygons.push_back(std::move(polygon7));
    mPolygons.push_back(std::move(polygon8));
    mPolygons.push_back(std::move(polygon9));
    mPolygons.push_back(std::move(polygon10));

}

void Stage::draw(SDL_Renderer *renderer) {
    for(auto item = mPolygons.begin(); item != mPolygons.end(); ++item)
        (*item)->draw(renderer);
}

void Stage::updateMousePos(int posX, int posY) {
}

int Stage::getStageWidth() {
    return mWidth;
}

int Stage::getStageHeight() {
    return mHeight;
}

Point Stage::findScreenEdgeIntersection(const Point &point, const int angle) {
    bool edgeIntersectionFound = false;
    Point edgeIntersectionCoords = point;
    int lineLength = 1;

    while(!edgeIntersectionFound)
    {
        double x2 = point.x + (lineLength * cos(angle * M_PI / 180.0));
        double y2 = point.y + (lineLength * sin(angle * M_PI / 180.0));
        lineLength++;
        if(x2 >= mWidth || x2 <= 0 || y2 >= mHeight || y2 <= 0) {
            edgeIntersectionFound = true;
            edgeIntersectionCoords = Point((int)round(x2), (int)round(y2));
        }
    }

    return edgeIntersectionCoords;
}

double Stage::distanceBetweenPoints(const Point &p1, const Point &p2) {
    return sqrt(pow((p2.x - p1.x), 2) + pow((p2.y - p1.y), 2));
}

LinearEquation Stage::calculateLinearEquation(const Point &point1, const Point &point2) {
    const int xSub = point2.x - point1.x;
    const int ySub = point2.y - point1.y;

    double y = xSub;
    double a = ySub;
    double b = (ySub * -point1.x) - (xSub * -point1.y);

    if(y == 0)
        return LinearEquation(0, 1, -b / a);
    else if(a == 0)
        return LinearEquation(1, 0, b / y);
    else
        return LinearEquation(1, a / y, b / y);
}

bool Stage::isParallel(const LinearEquation &equation1, const LinearEquation &equation2) {
    if(equation1.y == 0 && equation2.y == 0)
        return true;
    else if(equation1.a == 0 && equation2.a == 0)
        return true;
    else if(equation1.a != 0 && equation2.a != 0 && equation1.a == equation2.a)
        return true;

    return false;
}

bool Stage::calculateIntersection(const LinearEquation &equation1, const LinearEquation &equation2, Point &point) {
    if(isParallel(equation1, equation2))
        return false;

    if(equation1.a == 0 && equation2.y == 0 && equation1.y != 0 && equation2.a != 0)
    {
        point.x = (int)equation2.b;
        point.y = (int)equation1.b;
    }
    else if(equation1.y == 0 && equation2.a == 0 && equation1.a != 0 && equation2.y != 0)
    {
        point.x = (int)equation1.b;
        point.y = (int)equation2.b;
    }
    else if(equation2.a == 0 && equation2.y != 0 && equation1.a != 0 && equation1.y != 0)
    {
        point.x = (int)round(equation2.b - equation1.b);
        point.y = (int)equation2.b;
    }
    else if(equation1.a == 0 && equation1.y != 0 && equation2.a != 0 && equation2.y != 0)
    {
        point.x = (int)round((equation1.b - equation2.b) / (equation2.a - equation1.a));
        point.y = (int)equation1.b;
    }
    else if(equation2.y == 0 && equation2.a != 0 && equation1.a != 0 && equation1.y != 0)
    {
        point.x = (int)equation2.b;
        point.y = (int)round(equation2.b + equation1.b);
    }
    else if(equation1.y == 0 && equation1.a != 0 && equation2.a != 0 && equation2.y != 0)
    {
        point.x = (int)equation1.b;
        point.y = (int)round(equation2.a * point.x + equation2.b);
    }
    else {
        point.x = (int)round((equation1.b - equation2.b) / (equation2.a - equation1.a));
        point.y = (int)round(equation1.a * point.x + equation1.b);
    }


    return true;
}

Point Stage::findClosestIntersection(const Point &point, const int angle) {
    const Point edgeIntersectionPoint = findScreenEdgeIntersection(point, angle);
    double distance = distanceBetweenPoints(point, edgeIntersectionPoint);
    Point closestIntersection = edgeIntersectionPoint;

    double tmpDistance = 0;
    Point tmpIntersection(0, 0);
    for(auto currentPolygon = mPolygons.begin(); currentPolygon != mPolygons.end(); ++currentPolygon)
    {
        for(int i = 0; i < (*currentPolygon)->getEdgeCount(); i++)
        {
            const std::pair<Point, Point> edge = (*currentPolygon)->getEdge(i);

            auto firstEquation = calculateLinearEquation(point, edgeIntersectionPoint);
            auto secondEquation = calculateLinearEquation(edge.first, edge.second);

            bool found = calculateIntersection(firstEquation, secondEquation, tmpIntersection);
            if(found){
                if(!isBetween(tmpIntersection, edge.first, edge.second) || !isBetween(tmpIntersection, point, edgeIntersectionPoint))
                    continue;

                tmpDistance = distanceBetweenPoints(point, tmpIntersection);

                if(tmpDistance > distance)
                    continue;

                distance = tmpDistance;
                closestIntersection = tmpIntersection;
            }
        }
    }

    return closestIntersection;
}

bool Stage::isBetween(const Point &pointToCheck, const Point &point1, const Point &point2) {
    return pointToCheck.x >= std::min(point1.x, point2.x) && pointToCheck.x <= std::max(point1.x, point2.x)
           && pointToCheck.y >= std::min(point1.y, point2.y) && pointToCheck.y <= std::max(point1.y, point2.y);
}
