#ifndef SDL_RAYTRACER_POINT_H
#define SDL_RAYTRACER_POINT_H
#include <iostream>

struct Point {
    int x;
    int y;
    Point(int x, int y) : x(x), y(y) {}
    const std::string toString(){
        return "P(" + std::to_string(x) + ", " + std::to_string(y) + ")";
    }
};

#endif