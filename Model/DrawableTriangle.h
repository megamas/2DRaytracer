#ifndef SDL_RAYTRACER_TRIANGLE_H
#define SDL_RAYTRACER_TRIANGLE_H
#include <vector>
#include "Drawable.h"
#include "Point.h"

class DrawableTriangle : public Drawable {
protected:
    std::vector<Point> mVertices;
    int mDebugID;

public:
    DrawableTriangle(Point &vertex1, Point &vertex2, Point &vertex3, int debugID);
    DrawableTriangle(Point vertex1, Point vertex2, Point vertex3, int debugID);
    ~DrawableTriangle();

    virtual std::pair<Point, Point> getEdge(int edgeNum) override;
    virtual int getEdgeCount() override;
    virtual void draw(SDL_Renderer *renderer) override;

};


#endif //SDL_RAYTRACER_TRIANGLE_H
