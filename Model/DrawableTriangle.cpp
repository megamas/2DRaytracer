#include "DrawableTriangle.h"
#include "../Helpers/LogPrinter.h"

DrawableTriangle::DrawableTriangle(Point &vertex1, Point &vertex2, Point &vertex3, int debugID) : mDebugID(debugID){
    mVertices.push_back(vertex1);
    mVertices.push_back(vertex2);
    mVertices.push_back(vertex3);

    LogPrinter::print("Polygon(Triangle) no." + std::to_string(debugID) + " created...");
}

DrawableTriangle::DrawableTriangle(Point vertex1, Point vertex2, Point vertex3, int debugID) : mDebugID(debugID){
    mVertices.push_back(vertex1);
    mVertices.push_back(vertex2);
    mVertices.push_back(vertex3);

    LogPrinter::print("Polygon(Triangle) no." + std::to_string(debugID) + " created...");
}

DrawableTriangle::~DrawableTriangle() {
    LogPrinter::print("Polygon(Triangle) no." + std::to_string(mDebugID) + " destroyed...");
}

void DrawableTriangle::draw(SDL_Renderer *renderer) {
    SDL_RenderDrawLine(renderer, mVertices.at(0).x, mVertices.at(0).y, mVertices.at(mVertices.size() - 1).x, mVertices.at(mVertices.size() - 1).y);
    for(unsigned int i = 1; i < mVertices.size(); i++){
        SDL_RenderDrawLine(renderer, mVertices.at(i - 1).x, mVertices.at(i - 1).y, mVertices.at(i).x, mVertices.at(i).y);
    }
}

std::pair<Point, Point> DrawableTriangle::getEdge(int edgeNum) {
    if(edgeNum >= mVertices.size())
        throw std::out_of_range("DrawableTriangle::getEdge() : out of range");

    return std::pair<Point, Point>(mVertices.at((unsigned long)edgeNum), mVertices.at((unsigned long)((edgeNum + 1) % 3)));
}

int DrawableTriangle::getEdgeCount() {
    return (int)mVertices.size();
}
