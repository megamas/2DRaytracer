//
// Created by megamas on 3/24/18.
//

#ifndef SDL_RAYTRACER_DRAWABLELINE_H
#define SDL_RAYTRACER_DRAWABLELINE_H
#include "Drawable.h"

class DrawableLine : public Drawable {
protected:
    const Point mVertex1;
    const Point mVertex2;
    int mDebugID;

public:
    DrawableLine(Point &vertex1, Point &vertex2, int debugID);
    DrawableLine(Point vertex1, Point vertex2, int debugID);
    ~DrawableLine();

    virtual std::pair<Point, Point> getEdge(int edgeNum) override;
    virtual int getEdgeCount() override;
    virtual void draw(SDL_Renderer *renderer) override;
};


#endif //SDL_RAYTRACER_DRAWABLELINE_H
