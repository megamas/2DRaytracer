#ifndef SDL_RAYTRACER_WORLD_H
#define SDL_RAYTRACER_WORLD_H
#include <vector>
#include <memory>
#include "Drawable.h"
#include "Point.h"
#include "LinearEquation.h"

class Stage {
protected:
    std::vector<std::unique_ptr<Drawable>> mPolygons;
    int mWidth;
    int mHeight;

private:
    Point findScreenEdgeIntersection(const Point &point, const int angle);
//    Point findSegmentIntersection(const Point &p1, const Point &p2, const Point &p3, const Point &p4);
    double distanceBetweenPoints(const Point &p1, const Point &p2);

    LinearEquation calculateLinearEquation(const Point &point1, const Point &point2);
    bool isParallel(const LinearEquation &equation1, const LinearEquation &equation2);
    bool calculateIntersection(const LinearEquation &equation1, const LinearEquation &equation2, Point &point);

    bool isBetween(const Point &pointToCheck, const Point &point1, const Point &point2);
public:
    Stage(int witdh, int height);
    void updateMousePos(int posX, int posY);
    void draw(SDL_Renderer *renderer);
    int getStageWidth();
    int getStageHeight();

    Point findClosestIntersection(const Point &point, const int angle);
};


#endif //SDL_RAYTRACER_WORLD_H
