#ifndef SDL_RAYTRACER_DRAWABLE_H
#define SDL_RAYTRACER_DRAWABLE_H

#include <SDL2/SDL.h>
#include "Point.h"

class Drawable {
public:
    virtual void draw(SDL_Renderer *renderer) = 0;
    virtual std::pair<Point, Point> getEdge(int edgeNum) = 0;
    virtual int getEdgeCount() = 0;
    virtual ~Drawable(){}
};
#endif //SDL_RAYTRACER_DRAWABLE_H
