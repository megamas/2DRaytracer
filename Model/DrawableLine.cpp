//
// Created by megamas on 3/24/18.
//

#include "DrawableLine.h"
#include "DrawableTriangle.h"
#include "../Helpers/LogPrinter.h"

DrawableLine::DrawableLine(Point &vertex1, Point &vertex2, int debugID) : mVertex1(vertex1), mVertex2(vertex2), mDebugID(debugID){
    LogPrinter::print("Polygon(Line) no." + std::to_string(debugID) + " created...");
}

DrawableLine::DrawableLine(Point vertex1, Point vertex2, int debugID) : mVertex1(vertex1), mVertex2(vertex2), mDebugID(debugID){
    LogPrinter::print("Polygon(Line) no." + std::to_string(debugID) + " created...");
}

DrawableLine::~DrawableLine() {
    LogPrinter::print("Polygon(Line) no." + std::to_string(mDebugID) + " destroyed...");
}

void DrawableLine::draw(SDL_Renderer *renderer) {
    SDL_RenderDrawLine(renderer, mVertex1.x, mVertex1.y, mVertex2.x, mVertex2.y);
}

std::pair<Point, Point> DrawableLine::getEdge(int edgeNum) {
    if(edgeNum >= 1)
        throw std::out_of_range("DrawableTriangle::getEdge() : out of range");

    return std::pair<Point, Point>(mVertex1, mVertex2);
}

int DrawableLine::getEdgeCount() {
    return 1;
}
