#ifndef SDL_RAYTRACER_RAYCASTER_H
#define SDL_RAYTRACER_RAYCASTER_H
#include <SDL2/SDL.h>
#include <memory>
#include "Movable.h"
#include "Drawable.h"
#include "Stage.h"

class RayCaster : public Movable, public Drawable {
protected:
    std::shared_ptr<Stage> mStage;
    int mPosX;
    int mPosY;

public:
    RayCaster(std::shared_ptr<Stage> stage, int posX, int posY);
    void draw(SDL_Renderer *renderer) override;
    void updatePos(int posX, int posY) override;
    ~RayCaster() override;

    std::pair<Point, Point> getEdge(int edgeNum) override;
    int getEdgeCount() override;

};


#endif //SDL_RAYTRACER_RAYCASTER_H
