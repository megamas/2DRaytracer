#ifndef SDL_RAYTRACER_LINEAREQUATION_H
#define SDL_RAYTRACER_LINEAREQUATION_H

struct LinearEquation {
    double y;
    double a;
    double b;

    LinearEquation(double y, double a, double b){
        this->y = y;
        this->a = a;
        this->b = b;
    }
};

#endif //SDL_RAYTRACER_LINEAREQUATION_H
