#ifndef SDL_RAYTRACER_MOVABLE_H
#define SDL_RAYTRACER_MOVABLE_H


class Movable {
public:
    virtual void updatePos(int posX, int posY) = 0;
};


#endif //SDL_RAYTRACER_MOVABLE_H
