#ifndef SDL_RAYTRACER_MOUSEBUTTONS_H
#define SDL_RAYTRACER_MOUSEBUTTONS_H

enum class MouseButton {
    LEFT, MIDDLE, RIGHT
};

#endif
