#ifndef SDL_RAYTRACER_MYCONTROLLER_H
#define SDL_RAYTRACER_MYCONTROLLER_H

#include <memory>
#include "Controller.h"
#include "../Model/Stage.h"
#include "../Model/RayCaster.h"

class MyController : public Controller {
    std::shared_ptr<Stage> mStage;
    std::shared_ptr<RayCaster> mRayCaster;

public:
    MyController(std::shared_ptr<Stage> stage, std::shared_ptr<RayCaster> raycaster);

    virtual void onMouseMove(int posX, int posY) override;
    virtual void onMouseDown(int posX, int posY, MouseButton button) override;
    virtual void onMouseUp(int posX, int posY, MouseButton button) override;

    ~MyController() override;
};

#endif
