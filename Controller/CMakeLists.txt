#Controller
cmake_minimum_required(VERSION 3.7)
project(SDL_RayTracer)

set(CMAKE_CXX_STANDARD 14)
set(SOURCE_FILES
        MouseButton.h
        Controller.h
        MyController.h MyController.cpp)

add_library(Controller STATIC ${SOURCE_FILES})