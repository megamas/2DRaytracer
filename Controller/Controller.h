#ifndef SDL_RAYTRACER_CONTROLLER_H
#define SDL_RAYTRACER_CONTROLLER_H
#include "MouseButton.h"

class Controller {
public:
    virtual void onMouseDown(int posX, int posY, MouseButton button) = 0;
    virtual void onMouseUp(int posX, int posY, MouseButton button) = 0;
    virtual void onMouseMove(int posX, int posY) = 0;

    virtual ~Controller(){}
};

#endif
