#include "MyController.h"

#define DEBUG_MOUSE 0
#if DEBUG_MOUSE
#include <iostream>
#include "../Helpers/LogPrinter.h"
#endif

MyController::MyController(std::shared_ptr<Stage> stage, std::shared_ptr<RayCaster> raycaster)
        : mRayCaster(raycaster),
          mStage(stage)
{

}

void MyController::onMouseMove(int posX, int posY) {
#if DEBUG_MOUSE
    LogPrinter::print("Mouse moved, new position(x, y): (" + std::to_string(posX) + ", " + std::to_string(posY) + ")");
#endif

    mRayCaster->updatePos(posX, posY);
    mStage->updateMousePos(posX, posY);
}

void MyController::onMouseDown(int posX, int posY, MouseButton button) {
#if DEBUG_MOUSE
    LogPrinter::print("Mouse button pressed, position(x, y): (" + std::to_string(posX) + ", " + std::to_string(posY) + ")");
#endif
}

void MyController::onMouseUp(int posX, int posY, MouseButton button) {
#if DEBUG_MOUSE
    LogPrinter::print("Mouse button released, position(x, y): (" + std::to_string(posX) + ", " + std::to_string(posY) + ")");
#endif
}

MyController::~MyController() {

}
